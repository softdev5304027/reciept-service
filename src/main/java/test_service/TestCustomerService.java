/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author kitta
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cs =  new CustomerService();
        for(Customer customer :cs.getCustomers()) {
            System.out.println(customer);
        }
        Customer cus1 = new Customer("K","0879456123");
        for(Customer customer :cs.getCustomers()) {
            System.out.println(customer);
        }
        Customer delCus = cs.getByTel("0879456123");
        delCus.setTel("0879456123");
        cs.update(delCus);
        for(Customer customer :cs.getCustomers()) {
            System.out.println(customer);
        }
        cs.delete(delCus);
        for(Customer customer :cs.getCustomers()) {
            System.out.println(customer);
        }
    }
}
